package com.younginnovations.infomax.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.younginnovations.infomax.R;
import com.younginnovations.infomax.model.http.InfoListResponse;
import com.younginnovations.infomax.model.listener.ListItemClickListener;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QuotesRecyclerAdapter extends RecyclerView.Adapter<QuotesRecyclerAdapter.QuotesViewHolder> {

    private Context context;
    private List<InfoListResponse> infoListResponseList;
    public ListItemClickListener listItemClickListener;

    public QuotesRecyclerAdapter(Context context, List<InfoListResponse> infoListResponses){
        this.context = context;
        this.infoListResponseList = infoListResponses;
    }

    @Override
    public QuotesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quotes_recycler_row_item, parent,false);
        QuotesViewHolder albumViewHolder = new QuotesViewHolder(view);

        return albumViewHolder;
    }

    @Override
    public void onBindViewHolder(QuotesViewHolder holder, int position) {
        holder.quotesTitle.setText(infoListResponseList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return infoListResponseList.size();
    }

    public class QuotesViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.quotesTitle)
        TextView quotesTitle;
        @Bind(R.id.infoList)
        LinearLayout infoList;

        QuotesViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.infoList)
        public void onClickListItem(){
            listItemClickListener.onClickListItem(getAdapterPosition());
        }
    }
}
