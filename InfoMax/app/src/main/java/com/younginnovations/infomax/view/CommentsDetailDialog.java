package com.younginnovations.infomax.view;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.younginnovations.infomax.R;
import com.younginnovations.infomax.model.http.CommentsResponse;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CommentsDetailDialog extends DialogFragment {

    @Bind(R.id.name)
    TextView name;
    @Bind(R.id.email)
    TextView email;
    @Bind(R.id.details)
    TextView details;

    private CommentsResponse commentsResponse;

    public static CommentsDetailDialog newInstance(String flag, CommentsResponse commentsResponses) {
        CommentsDetailDialog f = new CommentsDetailDialog();
        Bundle args = new Bundle();
        args.putString("TEST", flag);
        args.putParcelable("RESPONSELIST", (Parcelable) commentsResponses);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.details_dialog_fragment, container);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().setTitle("Info List Details");
//        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        commentsResponse = getArguments().getParcelable("RESPONSELIST");
        name.setText("Name : "+commentsResponse.getName());
        email.setText("Email : "+commentsResponse.getEmail());
        details.setText("Details : "+commentsResponse.getBody());

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
