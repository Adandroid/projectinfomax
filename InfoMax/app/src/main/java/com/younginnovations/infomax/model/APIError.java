package com.younginnovations.infomax.model;


public class APIError {
    private String ErrorDescription;
    private String Message;

    public APIError() {
    }

    public APIError(String errorDescription, String message) {
        ErrorDescription = errorDescription;
        Message = message;
    }

    public String getErrorDescription() {
        return ErrorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        ErrorDescription = errorDescription;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
