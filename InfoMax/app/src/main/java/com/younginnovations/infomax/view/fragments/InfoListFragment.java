package com.younginnovations.infomax.view.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.younginnovations.infomax.R;
import com.younginnovations.infomax.model.APIError;
import com.younginnovations.infomax.model.ServiceGenerator;
import com.younginnovations.infomax.model.http.InfoListResponse;
import com.younginnovations.infomax.model.listener.InfoListRetrofitApi;
import com.younginnovations.infomax.model.listener.ListItemClickListener;
import com.younginnovations.infomax.util.Util;
import com.younginnovations.infomax.view.activity.MainActivity;
import com.younginnovations.infomax.view.adapter.QuotesRecyclerAdapter;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by suresh on 8/28/16.
 */
public class InfoListFragment extends Fragment implements ListItemClickListener{
    @Bind(R.id.quotesListRecyclerView)
    RecyclerView quotesListRecyclerView;

    private MaterialDialog progressDialog;
    private boolean networkAvailability;
    private List<InfoListResponse> infoListResponseList;
    private QuotesRecyclerAdapter quotesRecyclerAdapter;
    public SendListId sendListId;


    public interface SendListId {
        void onSendData(int listItemId);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_infolist, container, false);
        ButterKnife.bind(this, view);
        ((MainActivity)getActivity()).setToolbarText("Info List");
        if (infoListResponseList==null||infoListResponseList.isEmpty()) {
            networkAvailability = Util.isNetworkAvailable(getActivity());
            if (networkAvailability) {
                infoListApiResponse();
            } else {
                showAlertDialog("Connection Failed", "No Internet Connectivity. /n Please check your network settings");
            }
        }else
            setupQuotesRecyclerView();
        return view;
    }

    public void setupQuotesRecyclerView(){
        quotesListRecyclerView.setHasFixedSize(true);
        quotesListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        quotesRecyclerAdapter = new QuotesRecyclerAdapter(getActivity(),infoListResponseList);
        quotesRecyclerAdapter.listItemClickListener = this;
        quotesListRecyclerView.setAdapter(quotesRecyclerAdapter);
    }

    public void infoListApiResponse(){
        showProgressDialog();
        InfoListRetrofitApi infoListRetrofitApi = ServiceGenerator.createService(InfoListRetrofitApi.class);
        Call<List<InfoListResponse>> call = infoListRetrofitApi.getInfoList();
        call.enqueue(new Callback<List<InfoListResponse>>() {
            @Override
            public void onResponse(Call<List<InfoListResponse>> call, Response<List<InfoListResponse>> response) {
                if (response.isSuccessful()){
                    dismissDialog();
                    infoListResponseList = response.body();
                    setupQuotesRecyclerView();
                }else {
                    dismissDialog();
                    try {
                        Converter<ResponseBody, APIError> errorConverter =
                                ServiceGenerator.retrofit.responseBodyConverter(APIError.class, new Annotation[0]);
                        APIError error = errorConverter.convert(response.errorBody());
                        showAlertDialog("Connection Failed", error.getMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<InfoListResponse>> call, Throwable t) {
                dismissDialog();
                showAlertDialog("Sorry...", "Error fetching data from server");
            }
        });
    }

    public void showAlertDialog(String title, String message){
        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
    }

    public void showProgressDialog() {
        progressDialog = new MaterialDialog.Builder(getActivity())
                .title("Sending!")
                .content("Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
    }
    public void dismissDialog(){
        if(progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            sendListId = (SendListId) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement SendListId");
        }
    }

    @Override
    public void onClickListItem(int position) {
        sendListId.onSendData(infoListResponseList.get(position).getId());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
