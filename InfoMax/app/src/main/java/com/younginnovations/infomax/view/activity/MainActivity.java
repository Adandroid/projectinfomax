package com.younginnovations.infomax.view.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.younginnovations.infomax.R;
import com.younginnovations.infomax.model.listener.ListItemClickListener;
import com.younginnovations.infomax.view.fragments.CommentsListFragment;
import com.younginnovations.infomax.view.fragments.InfoListFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements InfoListFragment.SendListId  {

    @Bind(R.id.toolbar_text)
    TextView toolbarText;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.flContent)
    FrameLayout flContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        toolbarText.setText("Info List");
        OpenFragment(new InfoListFragment());
    }

    public void OpenFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.flContent, fragment);
        transaction.commit();
    }


    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.flContent);
        super.onBackPressed();
        if (fragment instanceof InfoListFragment)
            this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSendData(int listItemId) {
        CommentsListFragment commentsListFragment = new CommentsListFragment();
        Bundle args = new Bundle();
        args.putInt("ID", listItemId);
        commentsListFragment.setArguments(args);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.flContent, commentsListFragment);
        transaction.commit();
    }

    public void setToolbarText(String text){
        toolbarText.setText(text);
    }
}
