package com.younginnovations.infomax.model.listener;

/**
 * Created by suresh on 8/28/16.
 */
public interface ListItemClickListener {
    void onClickListItem(int position);
}
