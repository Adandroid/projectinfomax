
package com.younginnovations.infomax.model.http;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class CommentsResponse implements Parcelable {

    private Integer postId;
    private Integer id;
    private String name;
    private String email;
    private String body;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    protected CommentsResponse(Parcel in) {
        name = in.readString();
        email = in.readString();
        body = in.readString();
    }

    public static final Creator<CommentsResponse> CREATOR = new Creator<CommentsResponse>() {
        @Override
        public CommentsResponse createFromParcel(Parcel in) {
            return new CommentsResponse(in);
        }

        @Override
        public CommentsResponse[] newArray(int size) {
            return new CommentsResponse[size];
        }
    };

    /**
     * 
     * @return
     *     The postId
     */
    public Integer getPostId() {
        return postId;
    }

    /**
     * 
     * @param postId
     *     The postId
     */
    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The body
     */
    public String getBody() {
        return body;
    }

    /**
     * 
     * @param body
     *     The body
     */
    public void setBody(String body) {
        this.body = body;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(body);
    }
}
