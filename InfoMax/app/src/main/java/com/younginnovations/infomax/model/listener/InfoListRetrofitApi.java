package com.younginnovations.infomax.model.listener;

import com.younginnovations.infomax.model.http.CommentsResponse;
import com.younginnovations.infomax.model.http.InfoListResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by suresh on 8/26/16.
 */
public interface InfoListRetrofitApi {
    @GET("posts/")
    Call<List<InfoListResponse>> getInfoList();

    @GET("posts/{slugName}/comments/")
    Call<List<CommentsResponse>> getComments(@Path("slugName") String slugName) ;
}
