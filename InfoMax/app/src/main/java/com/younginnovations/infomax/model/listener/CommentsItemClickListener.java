package com.younginnovations.infomax.model.listener;

/**
 * Created by suresh on 8/29/16.
 */
public interface CommentsItemClickListener {
    void onClickComment(int position);
}
