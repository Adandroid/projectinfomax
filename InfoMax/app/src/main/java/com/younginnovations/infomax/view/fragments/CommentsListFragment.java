package com.younginnovations.infomax.view.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.younginnovations.infomax.R;
import com.younginnovations.infomax.model.APIError;
import com.younginnovations.infomax.model.ServiceGenerator;
import com.younginnovations.infomax.model.http.CommentsResponse;
import com.younginnovations.infomax.model.listener.CommentsItemClickListener;
import com.younginnovations.infomax.model.listener.InfoListRetrofitApi;
import com.younginnovations.infomax.util.Util;
import com.younginnovations.infomax.view.CommentsDetailDialog;
import com.younginnovations.infomax.view.activity.MainActivity;
import com.younginnovations.infomax.view.adapter.CommentsRecyclerAdapter;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by suresh on 8/28/16.
 */
public class CommentsListFragment extends Fragment implements CommentsItemClickListener {
    @Bind(R.id.quotesListRecyclerView)
    RecyclerView quotesListRecyclerView;

    private CommentsDetailDialog commentsDetailDialog;
    private MaterialDialog progressDialog;
    private boolean networkAvailability;
    private List<CommentsResponse> commentsResponseList;
    private CommentsRecyclerAdapter commentsRecyclerAdapter;
    private int infoListId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_infolist, container, false);
        ButterKnife.bind(this, view);
        ((MainActivity)getActivity()).setToolbarText("Comments");
        infoListId = getArguments().getInt("ID");
        if (commentsResponseList==null||commentsResponseList.isEmpty()) {
            networkAvailability = Util.isNetworkAvailable(getActivity());
            if (networkAvailability) {
                commentsListApiResponse();
            } else {
                showAlertDialog("Connection Failed", "No Internet Connectivity. /n Please check your network settings");
            }
        }else
            setupQuotesRecyclerView();
        return view;
    }

    public void commentsListApiResponse() {
        showProgressDialog();
        final InfoListRetrofitApi retrofitApi = ServiceGenerator.createService(InfoListRetrofitApi.class);
        Call<List<CommentsResponse>> call = retrofitApi.getComments(String.valueOf(infoListId));
        call.enqueue(new Callback<List<CommentsResponse>>() {
            @Override
            public void onResponse(Call<List<CommentsResponse>> call, Response<List<CommentsResponse>> response) {
                if (response.isSuccessful()) {
                    dismissDialog();
                    commentsResponseList = response.body();
                    setupQuotesRecyclerView();
                } else {
                    dismissDialog();
                    try {
                        Converter<ResponseBody, APIError> errorConverter =
                                ServiceGenerator.retrofit.responseBodyConverter(APIError.class, new Annotation[0]);
                        APIError error = errorConverter.convert(response.errorBody());
                        showAlertDialog("Connection Failed", error.getMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<CommentsResponse>> call, Throwable t) {
                dismissDialog();
                showAlertDialog("Sorry...", "Error fetching data from server");
            }
        });
    }

    public void setupQuotesRecyclerView() {
        quotesListRecyclerView.setHasFixedSize(true);
        quotesListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        commentsRecyclerAdapter= new CommentsRecyclerAdapter(getActivity(), commentsResponseList);
        commentsRecyclerAdapter.commentsItemClickListener = this;
        quotesListRecyclerView.setAdapter(commentsRecyclerAdapter);
    }

    public void showAlertDialog(String title, String message) {
        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
    }

    public void showProgressDialog() {
        progressDialog = new MaterialDialog.Builder(getActivity())
                .title("Sending!")
                .content("Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    public void dismissDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onClickComment(int position) {
        Toast.makeText(getActivity(), "Clicked", Toast.LENGTH_SHORT).show();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("TEST");
        if (prev != null) {
            fragmentTransaction.remove(prev);
        }
        fragmentTransaction.addToBackStack(null);

        commentsDetailDialog = CommentsDetailDialog.newInstance("flag", commentsResponseList.get(position));
        commentsDetailDialog.show(fragmentTransaction, "TEST");
    }
}
