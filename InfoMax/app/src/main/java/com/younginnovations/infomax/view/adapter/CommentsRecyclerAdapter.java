package com.younginnovations.infomax.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.younginnovations.infomax.R;
import com.younginnovations.infomax.model.http.CommentsResponse;
import com.younginnovations.infomax.model.listener.CommentsItemClickListener;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by suresh on 8/28/16.
 */
public class CommentsRecyclerAdapter extends RecyclerView.Adapter<CommentsRecyclerAdapter.CommentsViewHolder> {

    private Context context;
    private List<CommentsResponse> commentsResponseList;
    public CommentsItemClickListener commentsItemClickListener;

    public CommentsRecyclerAdapter(Context context, List<CommentsResponse> infoListResponses){
        this.context = context;
        this.commentsResponseList = infoListResponses;
    }

    @Override
    public CommentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quotes_recycler_row_item, parent,false);
        CommentsViewHolder albumViewHolder = new CommentsViewHolder(view);

        return albumViewHolder;
    }

    @Override
    public void onBindViewHolder(CommentsViewHolder holder, int position) {
        holder.quotesTitle.setText(commentsResponseList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return commentsResponseList.size();
    }

    public class CommentsViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.quotesTitle)
        TextView quotesTitle;
        @Bind(R.id.infoList)
        LinearLayout infoList;

        CommentsViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.infoList)
        public void onClickListItem(){
            commentsItemClickListener.onClickComment(getAdapterPosition());
        }
    }
}
